#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <assert.h>
#include <string.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_resource.h"
#include "disastrOS_descriptor.h"
#include "disastrOS_message.h"

int NUM_PROC ;

// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Hello, I am the sleeper, and I sleep %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    disastrOS_printStatus();
  }
}

void childFunction(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will iterate a bit, before terminating\n");
  
  int type=0;
  int mode=0;
  
  int fd_queue = 0 ;
  
  //TEST 1
  
  //figli con pid pari : scrivono
  //figli con pid dispari : leggono
  
  if ( disastrOS_getpid()%2 == 0 ) {
	  type = 1 ;
	  mode=DSOS_WRITE;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char string1[MAX_SIZE] ;
	  int j = snprintf(string1,MAX_SIZE,"Hi, i'm child %d " , disastrOS_getpid());
	  disastrOS_write(fd_queue,string1) ;
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA SCRITTURA!\n");
	  else if ( running->syscall_retvalue == DSOS_BUFFERSIZEWRONG ) printf("BUFFER TROPPO GRANDE!\n");
	  else if ( running->syscall_retvalue == DSOS_CODAPIENA ) disastrOS_write(fd_queue,string1) ;
  }
  else {
	  type = 1 ;
	  mode = DSOS_READ;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char b[MAX_SIZE] ;
	  disastrOS_read(fd_queue,b);
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA LETTURA\n");
	  else if ( running->syscall_retvalue == DSOS_EMPTYQUEUE ) disastrOS_read(fd_queue,b);
	  printf("questo è quanto ho letto: %s\n", b);
  }
  
  
  //TEST 2
  
  //figli con pid pari : leggono
  //figli con pid dispari : scrivono 
  
  /*if ( disastrOS_getpid()%2 != 0 ) {
	  type = 1 ;
	  mode=DSOS_WRITE;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char string1[MAX_SIZE] ;
	  int j = snprintf(string1,MAX_SIZE,"Hi, i'm child %d " , disastrOS_getpid());
	  disastrOS_write(fd_queue,string1) ;
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA SCRITTURA!\n");
	  else if ( running->syscall_retvalue == DSOS_BUFFERSIZEWRONG ) printf("BUFFER TROPPO GRANDE!\n");
	  else if ( running->syscall_retvalue == DSOS_CODAPIENA ) disastrOS_write(fd_queue,string1) ;
  }
  else {
	  type = 1 ;
	  mode = DSOS_READ;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char b[MAX_SIZE] ;
	  disastrOS_read(fd_queue,b);
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA LETTURA\n");
	  else if ( running->syscall_retvalue == DSOS_EMPTYQUEUE ) disastrOS_read(fd_queue,b);
	  printf("questo è quanto ho letto: %s\n", b);
  }*/
  
  //TEST 3
  
  //prima metà dei figli legge
  //il resto scrive
  
  /*if ( disastrOS_getpid() > NUM_PROC/2 ) {
	  type = 1 ;
	  mode=DSOS_WRITE;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char string1[MAX_SIZE] ;
	  int j = snprintf(string1,MAX_SIZE,"Hi, i'm child %d " , disastrOS_getpid());
	  disastrOS_write(fd_queue,string1) ;
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA SCRITTURA!\n");
	  else if ( running->syscall_retvalue == DSOS_BUFFERSIZEWRONG ) printf("BUFFER TROPPO GRANDE!\n");
	  else if ( running->syscall_retvalue == DSOS_CODAPIENA ) disastrOS_write(fd_queue,string1) ;
  }
  else {
	  type = 1 ;
	  mode = DSOS_READ;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char b[MAX_SIZE] ;
	  disastrOS_read(fd_queue,b);
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA LETTURA\n");
	  else if ( running->syscall_retvalue == DSOS_EMPTYQUEUE ) disastrOS_read(fd_queue,b);
	  printf("questo è quanto ho letto: %s\n", b);
  }*/
  
  //TEST 4
  
  //prima metà dei figli scrive
  //il resto legge
  
  /*if ( disastrOS_getpid() <= NUM_PROC/2 + 1 ) {
	  type = 1 ;
	  mode=DSOS_WRITE;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char string1[MAX_SIZE] ;
	  int j = snprintf(string1,MAX_SIZE,"Hi, i'm child %d " , disastrOS_getpid());
	  disastrOS_write(fd_queue,string1) ;
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA SCRITTURA!\n");
	  else if ( running->syscall_retvalue == DSOS_BUFFERSIZEWRONG ) printf("BUFFER TROPPO GRANDE!\n");
	  else if ( running->syscall_retvalue == DSOS_CODAPIENA ) disastrOS_write(fd_queue,string1) ;
  }
  else {
	  type = 1 ;
	  mode = DSOS_READ;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char b[MAX_SIZE] ;
	  disastrOS_read(fd_queue,b);
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA LETTURA\n");
	  else if ( running->syscall_retvalue == DSOS_EMPTYQUEUE ) disastrOS_read(fd_queue,b);
	  printf("questo è quanto ho letto: %s\n", b);
  }*/
  
  //TEST 5
  
  //abbiamo piu' figli che leggono rispetto a quelli che scrivono
  
  /*if ( disastrOS_getpid() < NUM_PROC/2 ) {
	  type = 1 ;
	  mode=DSOS_WRITE;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char string1[MAX_SIZE] ;
	  int j = snprintf(string1,MAX_SIZE,"Hi, i'm child %d " , disastrOS_getpid());
	  disastrOS_write(fd_queue,string1) ;
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA SCRITTURA!\n");
	  else if ( running->syscall_retvalue == DSOS_BUFFERSIZEWRONG ) printf("BUFFER TROPPO GRANDE!\n");
	  else if ( running->syscall_retvalue == DSOS_CODAPIENA ) disastrOS_write(fd_queue,string1) ;
  }
  else {
	  type = 1 ;
	  mode = DSOS_READ;
	  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
	  char b[MAX_SIZE] ;
	  disastrOS_read(fd_queue,b);
	  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA LETTURA\n");
	  else if ( running->syscall_retvalue == DSOS_EMPTYQUEUE ) disastrOS_read(fd_queue,b);
	  printf("questo è quanto ho letto: %s\n", b);
  }*/
  
  //TEST 6
  
  //comunicazione su più code
  
  /*int halfway = NUM_PROC/2 ;
  
  if ( disastrOS_getpid() <= halfway/2 || ( disastrOS_getpid() > halfway && disastrOS_getpid() < halfway + halfway/2 ) ) {
	  if ( disastrOS_getpid()%2 == 0 ) {
		  type = 1 ;
		  mode=DSOS_WRITE;
		  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
		  char string1[MAX_SIZE] ;
		  int j = snprintf(string1,MAX_SIZE,"Hi, i'm child %d " , disastrOS_getpid());
		  disastrOS_write(fd_queue,string1) ;
		  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA SCRITTURA!\n");
		  else if ( running->syscall_retvalue == DSOS_BUFFERSIZEWRONG ) printf("BUFFER TROPPO GRANDE!\n");
		  else if ( running->syscall_retvalue == DSOS_CODAPIENA ) disastrOS_write(fd_queue,string1) ;
	  }
	  else {
		  type = 1 ;
		  mode = DSOS_READ;
		  int fd_queue = disastrOS_openResource(QUEUE_ID_1,type,mode);
		  char b[MAX_SIZE] ;
		  disastrOS_read(fd_queue,b);
		  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA LETTURA\n");
		  else if ( running->syscall_retvalue == DSOS_EMPTYQUEUE ) disastrOS_read(fd_queue,b);
		  printf("questo è quanto ho letto: %s\n", b);
	  }
  }
  else {
	  if ( disastrOS_getpid()%2 == 0 ) {
		  type = 1 ;
		  mode=DSOS_WRITE;
		  int fd_queue = disastrOS_openResource(QUEUE_ID_2,type,mode);
		  char string1[MAX_SIZE] ;
		  int j = snprintf(string1,MAX_SIZE,"Hi, i'm child %d " , disastrOS_getpid());
		  disastrOS_write(fd_queue,string1) ;
		  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA SCRITTURA!\n");
		  else if ( running->syscall_retvalue == DSOS_BUFFERSIZEWRONG ) printf("BUFFER TROPPO GRANDE!\n");
		  else if ( running->syscall_retvalue == DSOS_CODAPIENA ) disastrOS_write(fd_queue,string1) ;
	  }
	  else {
		  type = 1 ;
		  mode = DSOS_READ;
		  int fd_queue = disastrOS_openResource(QUEUE_ID_2,type,mode);
		  char b[MAX_SIZE] ;
		  disastrOS_read(fd_queue,b);
		  if ( running->syscall_retvalue == DSOS_MODEWRONG ) printf("ERRORE NELLA LETTURA\n");
		  else if ( running->syscall_retvalue == DSOS_EMPTYQUEUE ) disastrOS_read(fd_queue,b);
		  printf("questo è quanto ho letto: %s\n", b);
	  }
  }*/
  
  
	  
  
  
  
  
  printf("PID: %d, terminating\n", disastrOS_getpid());
  disastrOS_closeResource(fd_queue);
  disastrOS_exit(disastrOS_getpid()+1);
}


void initFunction(void* args) {
  disastrOS_printStatus();
  printf("hello, I am init and I just started\n");
  
  
  disastrOS_spawn(sleeperFunction, 0);
  
  int type = 1;
  
  int fd_queue_1 = disastrOS_openResource(QUEUE_ID_1,type,DSOS_CREATE);
  int fd_queue_2 = disastrOS_openResource(QUEUE_ID_2,type,DSOS_CREATE);
  
  printf("I feel like to spawn 10 nice threads\n");
  int alive_children=0;
  type = 0 ;
  for (int i=0; i<NUM_PROC; ++i) {
	disastrOS_spawn(childFunction, 0);
	alive_children++;
  }	
	
  disastrOS_printStatus();
  int retval;
  int pid;
  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
    disastrOS_printStatus();
    printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
	   pid, retval, alive_children);
    --alive_children;
  }
  disastrOS_closeResource(fd_queue_1);
  disastrOS_closeResource(fd_queue_2);
  disastrOS_destroyResource(QUEUE_ID_1);
  disastrOS_destroyResource(QUEUE_ID_2);
  printf("shutdown!");
  disastrOS_shutdown();
}

int main(int argc, char** argv){
  char* logfilename=0;
  if (argc>1) {
    logfilename=argv[1];
  }
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  printf("the function pointer is: %p", childFunction);
  // spawn an init process
  printf("start\n");
  disastrOS_start(initFunction, 0, logfilename);
  return 0;
}
