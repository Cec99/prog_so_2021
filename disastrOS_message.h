#pragma once
#include "disastrOS_pcb.h"
#include "disastrOS_constants.h"
#include "disastrOS_resource.h"

typedef struct  {
  ListItem list;
  char buffer[MAX_SIZE];
} MessageItem;

typedef ListHead message_queue;

typedef struct {
	Resource res;
	message_queue mq;
} MQ ;

//funzioni per la queue dei messaggi

void Message_init() ;

MessageItem* MessageItem_alloc();

void MessageQueue_print(ListHead* messages);

MessageItem* MessageQueue_add(message_queue* messages,char* buffer);

MessageItem* MessageQueue_removeCurrent(ListHead* messages, ListItem* elem);

MessageItem* MessageItem_find(message_queue* head, int elem) ;

void MQ_init();

MQ* MQ_alloc(int id, int type) ;

int MessageItem_free(MessageItem* item);






