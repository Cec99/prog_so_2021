#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "disastrOS_timer.h"
#include "pool_allocator.h"
#include "disastrOS_pcb.h"
#include "disastrOS_message.h"

#define MESSAGE_ITEM_SIZE sizeof(MessageItem)
#define MESSAGE_QUEUE_SIZE sizeof(MQ)
#define MESSAGE_ITEM_MEMSIZE (sizeof(MessageItem)+sizeof(int))
#define MESSAGE_QUEUE_MEMSIZE (sizeof(MQ)+sizeof(int))
#define MESSAGE_ITEM_BUFFER_SIZE MAX_NUM_MESSAGES_PER_CODA*MESSAGE_ITEM_MEMSIZE
#define MESSAGE_QUEUE_BUFFER_SIZE MAX_NUM_CODE*MESSAGE_QUEUE_MEMSIZE

static char _message_item_buffer[MESSAGE_ITEM_BUFFER_SIZE];
static PoolAllocator _message_item_allocator;

static char _queue_buffer[MESSAGE_QUEUE_BUFFER_SIZE];
static PoolAllocator _queue_allocator;

void Message_init(){
  int result=PoolAllocator_init(& _message_item_allocator,
				MESSAGE_ITEM_SIZE,
				MAX_NUM_MESSAGES_PER_CODA,
				_message_item_buffer,
				MESSAGE_ITEM_BUFFER_SIZE);
  assert(! result);
}

MessageItem* MessageItem_alloc(){
  MessageItem* item=(MessageItem*) PoolAllocator_getBlock(&_message_item_allocator);
  if (! item) {
	  printf("\n\n");
	  printf("IL PROBLEMA E' NELL'ALLOCAZIONE DELL'ITEM \n\n");
	  printf("\n\n");
	  return NULL;
  }
  item->list.prev=0;
  item->list.next=0;
  return item;
}

void MQ_init() {
	int result=PoolAllocator_init(& _queue_allocator,
				MESSAGE_QUEUE_SIZE,
				MAX_NUM_CODE,
				_queue_buffer,
				MESSAGE_QUEUE_BUFFER_SIZE);
	assert(! result);
}

void message_queue_init(ListHead* messages) {
	List_init(messages);
}
	

MQ* MQ_alloc(int id, int type) {
	MQ* queue = (MQ*)PoolAllocator_getBlock(&_queue_allocator);
	if(!queue) return 0;
	queue->res.list.prev=queue->res.list.next=0;
	queue->res.id=id;
	queue->res.type=type;
	List_init(&queue->res.descriptors_ptrs);
	List_init(&queue->mq);
	return queue;
}


int MessageItem_free(MessageItem* item){
  return PoolAllocator_releaseBlock(&_message_item_allocator, (void*) item);
}


MessageItem* MessageQueue_add(message_queue* messages,char* buffer){
  if ( strlen(buffer) > MAX_SIZE ) {
	  return NULL ;
  }
  MessageItem* new_item=MessageItem_alloc();
  if (! new_item) {
	  printf("\n\n");
	  printf("PROBLEMA NELLA ADD\n");
	  printf("\n\n");
	  return NULL;
  }
  for ( int i=0 ; i<strlen(buffer) ; i++ ) {
	  new_item->buffer[i] = buffer[i] ;
  }
  List_insert(messages, messages->last, (ListItem*)new_item);
  return new_item;
}

MessageItem* MessageQueue_removeCurrent(ListHead* messages, ListItem* elem){
  MessageItem* item = (MessageItem*) List_detach(messages, elem);
  if (! item) return NULL;
  return item;
}

