#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_resource.h"
#include "disastrOS_descriptor.h"
#include "disastrOS_message.h"

void internal_write() {
	
	int fd = running->syscall_args[0];
	char* string = (char*)running->syscall_args[1];
	
	
	Descriptor* des=DescriptorList_byFd(&running->descriptors, fd);
	if ( des->mode != DSOS_WRITE) {
		running->syscall_retvalue = DSOS_MODEWRONG ;
	}
	MQ* queue = (MQ*)des->resource ;
	if ( queue->mq.size == MAX_NUM_MESSAGES_PER_CODA ) {
		running->status=Waiting;
		List_insert(&waiting_list, waiting_list.last, (ListItem*) running);
	
		PCB* next_running = (PCB*)List_detach(&ready_list,ready_list.first);
		running->syscall_retvalue = DSOS_CODAPIENA ;
		running = next_running ;
		return ;
	}
	
	int size_prec = queue->mq.size;
	

	
	MessageItem* mi = MessageQueue_add(&queue->mq,string);	
	
	if ( mi == NULL ) running->syscall_retvalue = DSOS_BUFFERSIZEWRONG ;
	
	printf(" ho scritto %s nella coda con id %d che ha size %d\n", mi->buffer , des->resource->id, queue->mq.size);
	ListItem* aux = (ListItem*)waiting_list.first ;
	
	
	
	
	if ( aux != NULL  && size_prec == 0 ) {
		int correct = 0 ;
		while ( correct == 0 && aux != NULL ) {
			Descriptor* des_w = DescriptorList_byFd(&((PCB*)aux)->descriptors, ((PCB*)aux)->syscall_args[0]);
			if (  des_w->resource->id == des->resource->id && des_w->mode == DSOS_READ ) {
				correct = 1 ;
				PCB* prescelto = (PCB*)List_detach(&waiting_list,aux);
				List_insert(&ready_list, NULL, (ListItem*) prescelto);
				return ;
			}
			aux = aux->next ;
		}
	}
}
