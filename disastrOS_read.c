#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_resource.h"
#include "disastrOS_descriptor.h"
#include "disastrOS_message.h"

void internal_read() {

	int fd = running->syscall_args[0];
	char* buffer = (char*)running->syscall_args[1];
	

	Descriptor* des=DescriptorList_byFd(&running->descriptors, fd);
	if ( des->mode != DSOS_READ ) {
		running->syscall_retvalue = DSOS_MODEWRONG ;
	}
	MQ* queue = (MQ*)des->resource ;
	if ( queue->mq.size == 0 ) {
		running->status=Waiting;
		List_insert(&waiting_list, waiting_list.last, (ListItem*) running);
		PCB* next_running = (PCB*)List_detach(&ready_list,ready_list.first);
		running->syscall_retvalue = DSOS_EMPTYQUEUE ;
		running = next_running ;
		return ;
	}
	
	int size_prec = queue->mq.size;
	
	MessageItem* mi = MessageQueue_removeCurrent(&queue->mq, queue->mq.first);
	
	strcpy(buffer,mi->buffer);
	
	printf("ho letto %s nella coda con id %d che ha size dopo la lettura %d\n", buffer , des->resource->id , queue->mq.size );
	
	MessageItem_free(mi);
	
	ListItem* aux = waiting_list.first ;
	
	if ( aux && size_prec == MAX_NUM_MESSAGES_PER_CODA ) {
		int correct = 0 ;
		while ( correct == 0 && aux != NULL ) {
			Descriptor* des_w = DescriptorList_byFd(&((PCB*)aux)->descriptors, ((PCB*)aux)->syscall_args[0]);
			if (  des_w->resource->id == des->resource->id && des_w->mode == DSOS_WRITE ) {
				correct = 1 ;
				PCB* prescelto = (PCB*)List_detach(&waiting_list,aux);
				List_insert(&ready_list, NULL, (ListItem*) prescelto);
				return ;
			}
			aux = aux->next ;
		}
	}
}
